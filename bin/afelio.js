#!/usr/bin/env node

/**
 *  @TODO If a block is not defined, just skip it (missing styles in the design.afelio file)
 *
 *
 **/

const path          =   require('path');
global.__base = path.resolve(__dirname + '/../');
global.__configFileName = 'design';
global.__configFileExtension = '.afelio';

const gulp          =   require('gulp');
const commander     =   require('commander');
const chalk         =   require('chalk');
const config        =   require( __base + '/helpers/config').load();
const task          =   new (require( __base + '/helpers/task'))(config, gulp, commander);
const pkginfo       =   require('pkginfo')(module, 'version');

/**
 *  Commander
 **/
commander.version( module.exports.version );


/**
 *  Actions
 **/
task.register('/lib/init');
task.register('/lib/build.dev');
task.register('/lib/build.prod');
task.register('/lib/clean');
task.register('/lib/connect');
task.register('/lib/watch');
task.register('/lib/config');

task.register('/lib/libs.prod');
task.register('/lib/libs.dev');

task.register('/lib/styles.prod');
task.register('/lib/styles.dev');

task.register('/lib/statics.default');

task.register('/lib/views.prod');
task.register('/lib/views.dev');

/**
 *  Improved help :)
 **/
commander.help = function()
{
  console.log('');
  console.log( require(__base + '/helpers/logo') );
  commander.outputHelp();
  process.exit();
}

/**
 *  Unknown command
 **/
commander.action( () => {console.log( chalk.red( process.argv[2]+" is not an afelio command. See 'afelio --help'") )} );

/**
 *  Known command
 **/
commander.parse(process.argv);

/**
 *  No command
 **/
(commander.args.length == 0) && commander.help();

/**
 *  View Task
 *
 *  @version 2.1
 *  @author Alexandre Masy <hello@alexandremasy.com>
 **/
const AbstractIteratorTask   =   require( __base + '/lib/abstract.iterator');

class AbstractViewTask extends AbstractIteratorTask
{
  /**
   *  Constructor
   *
   *  @param {Config} config
   *  @param {Gulp} gulp
   **/
  constructor(config, gulp)
  {
    super(config, gulp);

    this.pug = require('pug');
    this.pug.filters.code = this.codeFilter;
  }

  /**
   *  Pug Filter :code
   *  Code filter to be able to add code documentation in a pug file
   *
   *  @param block String
   **/
  codeFilter(block)
  {
    return block
    .replace( /&/g, '&amp;'  )
    .replace( /</g, '&lt;'   )
    .replace( />/g, '&gt;'   )
    .replace( /"/g, '&quot;' )
    .replace( /#/g, '&#35;'  )
    .replace( /\\/g, '\\\\'  )
    .replace( /\n/g, '<br>'   );
  }

  /**
   *  Execute the task
   **/
  execute()
  {
    super.execute();
    this.iterate( 'views' );
  }
}

module.exports = AbstractViewTask;

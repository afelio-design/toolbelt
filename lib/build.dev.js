/**
 *  Build Dev
 *
 *  @version 2.0
 *  @author Alexandre Masy <hello@alexandremasy.com>
 **/
const AbstractTask    =   require( __base + '/lib/abstract');
const TaskName        =   require( __base + '/helpers/TaskName' );

/**
 *  View Task
 **/
class Task extends AbstractTask
{
  /**
   *  Constructor
   *
   *  @param {Config} config
   *  @param {Gulp} gulp
   **/
  constructor(config, gulp)
  {
    super(config, gulp);
  }

  /**
  *  Return the list of dependencies
  *
  *  @return Array.<String>
  **/
  get deps() { return [ TaskName.VIEWS_DEV, TaskName.STYLES_DEV, TaskName.LIBS_DEV, TaskName.STATICS, TaskName.CONNECT, TaskName.WATCH ]; };

  /**
  *  Return the description of the task
  *
  *  @return String
  **/
  get description() { return 'Build the development version of the project'; };

  /**
  *  Return the name of the task
  *
  *  @return String
  **/
  get name() { return TaskName.BUILD_DEV; };

  /**
  *  Execute the task
  *
  *  @TODO handle exceptions
  **/
  execute(){}
}

module.exports = Task;

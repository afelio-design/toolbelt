/**
 *  Styles lint
 *
 *  @version 2.0
 *  @author Alexandre Masy <hello@alexandremasy.com>
 **/
class Task extends require( __base + '/lib/abstract.styles')
{
  /**
   *  Constructor
   *
   *  @param {Config} config
   *  @param {Gulp} gulp
   **/
  constructor(config, gulp)
  {
    super(config, gulp);
  }

  /**
  *  Return the description of the task
  *
  *  @return String
  **/
  get description(){ return 'Lint the styles files'; };

  /**
  *  Return the name of the task
  *
  *  @return String
  **/
  get name(){ return require( __base + '/helpers/TaskName' ).STYLES_LINT; };

  /**
  *  Iterator Executor
  *  Execute the action over an iterator
  *
  *  @param value Object
  **/
  iterator( value )
  {
    if (!(value.enabled == undefined ? true : JSON.parse(value.enabled)))
    {
      p.warn('Skipping due to flag `enabled` present');
      return;
    }

    var sources = value.sources;
    if (!sources || sources.length == 0)
    {
      p.error('Error: No sources for the styles');
    }

    const sassLint            =   require('gulp-sass-lint');

    return this.gulp.src(sources, {cwd:this.globalRoot})
    .pipe(sassLint())
    .pipe(sassLint.format());
  }
}

module.exports = Task;

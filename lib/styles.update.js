/**
 *  Styles Update
 *
 *  @version 2.0
 *  @author Alexandre Masy <hello@alexandremasy.com>
 **/
const AbstractTask    =   require( __base + '/lib/abstract');
const util            =   require('util');
const TaskName        =   require( __base + '/helpers/TaskName' );

/**
 *  View Task
 **/
class Task extends AbstractTask
{
  /**
   *  Constructor
   *
   *  @param {Config} config
   *  @param {Gulp} gulp
   **/
  constructor(config, gulp)
  {
    super(config, gulp);
  }

  /**
  *  Return the name of the task
  *
  *  @return String
  **/
  get name(){ return TaskName.STYLES_UPDATE; };

  /**
  *  Return the description of the task
  *
  *  @return String
  **/
  get description(){ return 'Build via update the styles for development'; };

  /**
  *  Execute the task
  *
  *  @TODO handle exceptions
  **/
  execute()
  {
    const sass            =   require('gulp-sass');
    const autoprefixer    =   require('gulp-autoprefixer');
    const plumber         =   require('gulp-plumber');
    const path            =   require('path');
    const changed         =   require('gulp-changed');
    const connect         =   require('gulp-connect');

    var sources = this.config.get('styles:sources');
    var output = path.join( this.globalOutput, this.config.get('styles:output') );
    var config = {};
    config.sourcemap = true;

    return this.gulp.src(sources, {cwd:this.globalRoot})
    .pipe(plumber({errorHandler:this.onError}))
    .pipe(changed(outputDir, {extension:".css"}))
    .pipe(sass(config))
    .pipe(autoprefixer())
    .pipe(this.gulp.dest(output))
    .pipe(connect.reload());
  }
}

module.exports = Task;

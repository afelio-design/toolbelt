/**
 *  Views Update
 *
 *  @version 2.0
 *  @author Alexandre Masy <hello@alexandremasy.com>
 **/
const AbstractViewTask    =   require( __base + '/lib/abstract.views');
const util                =   require('util');
const TaskName            =   require( __base + '/helpers/TaskName' );

/**
 *  View Task
 **/
class Task extends AbstractViewTask
{
  /**
   *  Constructor
   *
   *  @param {Config} config
   *  @param {Gulp} gulp
   **/
  constructor(config, gulp)
  {
    super(config, gulp);
  }

  /**
  *  Return the description of the task
  *
  *  @return String
  **/
  get description(){ return 'Build via update the views for production'; };

  /**
  *  Return the name of the task
  *
  *  @return String
  **/
  get name(){ return TaskName.VIEWS_UPDATE; };

  /**
  *  Iterator Executor
  *  Execute the action over an iterator
  *
  *  @param value Object
  **/
  iterator( value )
  {
    if (!(value.enabled == undefined ? true : JSON.parse(value.enabled)))
    {
      p.warn('Skipping due to flag `enabled` present');
      return;
    }

    const path                =   require('path');
    var sources = value.sources;

    var output = value.output || '';
    output = path.join( this.globalOutput, output );

    if (!sources || sources.length == 0)
    {
      p.error('Error: No sources for the styles');
    }

    const gulpPug             =   require('gulp-pug');
    const plumber             =   require('gulp-plumber');
    const changed             =   require('gulp-changed');
    const connect             =   require('gulp-connect');

    return this.gulp.src(sources, {cwd:this.globalRoot})
    .pipe(plumber({errorHandler:this.onError}))
    .pipe(changed(output, {extension:".html"}))
    .pipe(gulpPug({pug: this.pug, "pretty":true}))
    .pipe(this.gulp.dest(output))
    .pipe(connect.reload());
  }
}

exports = module.exports = Task;

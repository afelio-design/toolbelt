/**
 *  View Task
 *
 *   @version 2.1
 *   @author Alexandre Masy <hello@alexandremasy.com>
 **/
const AbstractTask    =   require( __base + '/lib/abstract');

class AbstractIteratorTask extends AbstractTask
{
  /**
   *  Constructor
   *
   *  @param {Config} config
   *  @param {Gulp} gulp
   **/
  constructor(config, gulp)
  {
    super(config, gulp);
  }

  /**
  *  Execute the task
  **/
  iterate( key, iterator )
  {
    var data = this.config.get( key );
    var iterator = iterator == undefined ? this.iterator : iterator;

    // Is the data declaration an object or an array
    if (data.length == undefined)
    {
      return iterator.call( this, data );
    }
    else
    {
      return data.map( iterator, this );
    }
  }

  /**
  *  Iterator Execution
  *  Execute the action over an iterator
  *
  *  @param value Object
  **/
  iterator( value )
  {

  }
}

module.exports = AbstractIteratorTask;

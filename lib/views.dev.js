/**
 *  View Task
 *
 *  @version 2.1
 *  @author Alexandre Masy <hello@alexandremasy.com>
 **/
class Task extends require( __base + '/lib/abstract.views')
{
  /**
   *  Constructor
   *
   *  @param {Config} config
   *  @param {Gulp} gulp
   **/
  constructor(config, gulp)
  {
    super(config, gulp);
    this.pug.filters.rebase = this.rebase.bind(this);
  }

  /**
   *  Return the description of the task
   *
   *  @return String
   **/
  get description() { return 'Build the views for development'; };

  /**
   *  Return the name of the task
   *
   *  @return String
   **/
  get name() { return require( __base + '/helpers/TaskName' ).VIEWS_DEV; };


  /**
   *  Execute the task
   **/
  execute()
  {
    super.execute();
    require('gulp-connect').reload();
  }

  /**
   *  Iterator Executor
   *  Execute the action over an iterator
   *
   *  @param value Object
   **/
  iterator( value )
  {
    if (!(value.enabled == undefined ? true : JSON.parse(value.enabled)))
    {
      p.warn('Skipping due to flag `enabled` present');
      return;
    }

    let sources = value.sources;
    let basedir = value.basedir || __dirname;
    let output = value.output || '';
    output = require('path').join( this.globalOutput, output );

    if (!sources || sources.length == 0)
    {
      p.error('Error: No sources for the styles');
    }

    return this.gulp.src(sources, {cwd:this.globalRoot})
                    .pipe(require('gulp-plumber')({errorHandler:this.onError}))
                    .pipe(this.pugEngine(basedir))
                    .pipe(this.gulp.dest(output));
  }

  /**
   *  Return the pug engine
   *
   *  @param {String} basedir
   *  @return gulp-pug
   **/
  pugEngine(basedir)
  {
    let plugins = [require('../helpers/pugjs.basedir')({paths: basedir})];
    let filters = {'rebase':this.rebase.bind(this)};
    let locals = this.config.get('global');
    locals.rebase = '/';

    return require('gulp-pug')({
      pug: this.pug,
      locals: locals,
      pretty: true,
      plugins: plugins,
      filters: filters
    });
  }

  /**
   *  Rebase the website via the url in the configuration file
   *
   *  @param {String} url
   *  @return {String}
   **/
  rebase(url)
  {
    var port = this.config.get( 'global:port' ) || "8080";
    var host = this.config.get( 'global:host' ) || "localhost"

    let ret = '\n\t\t';
    ret += '<base href="http://'+ host +':'+port+'/';
    ret += '" target="_self">'

    return ret;
  }
}

module.exports = Task;

/**
 *  Libs Prod
 *
 *  @version 2.0
 *  @author Alexandre Masy <hello@alexandremasy.com>
 **/
const AbstractLibsTask    =   require( __base + '/lib/abstract.libs');
const TaskName            =   require( __base + '/helpers/TaskName' );

/**
 *  View Task
 **/
class Task extends AbstractLibsTask
{
  /**
   *  Constructor
   *
   *  @param {Config} config
   *  @param {Gulp} gulp
   **/
  constructor(config, gulp)
  {
    super(config, gulp);
  }

  /**
  *  Return the aliases for the task
  *
  *  @return String
  **/
  get aliases() { return TaskName.LIBS_PROD; };

  /**
  *  Return the description of the task
  *
  *  @return String
  **/
  get description() { return 'Build the application libraries for production'; };

  /**
  *  Return the name of the task
  *
  *  @return String
  **/
  get name() { return TaskName.LIBS; };

  /**
  *  Iterator Executor
  *  Execute the action over an iterator
  *
  *  @param value Object
  **/
  iterator( value )
  {
    const path                =   require('path');

    if (!(value.enabled == undefined ? true : JSON.parse(value.enabled)))
    {
      p.warn('Skipping due to flag `enabled` present');
      return;
    }

    var name = value.name;
    var sources = value.sources;
    var output = value.output || '';
    output = path.join( this.globalOutput, output );

    if (!name)
    {
      p.error('Error: The name of the library is missing');
    }

    if (!sources || sources.length == 0)
    {
      p.error('Error: No sources for the library');
    }

    const plumber             =   require('gulp-plumber');
    const uglify              =   require('gulp-uglify');
    const concat              =   require('gulp-concat');

    return this.gulp.src(sources, {cwd:this.globalRoot})
    .pipe(plumber({errorHandler:this.onError}))
    .pipe(concat(name))
    .pipe(uglify())
    .pipe(this.gulp.dest(output));
  }
}

module.exports = Task;

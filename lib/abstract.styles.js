/**
 *  Abstract Styles Task
 *
 *  @version 2.0
 *  @author Alexandre Masy <hello@alexandremasy.com>
 **/
const AbstractTask    =   require( __base + '/lib/abstract.iterator');

/**
 *  View Task
 **/
class AbstractStylesTask extends AbstractTask
{
  /**
   *  Constructor
   *
   *  @param {Config} config
   *  @param {Gulp} gulp
   **/
  constructor(config, gulp)
  {
    super(config, gulp);
  }

  /**
  *  Execute the task
  **/
  execute()
  {
    super.execute();
    this.iterate( 'styles' );
  }

  /**
  *  Iterator Executor
  *  Execute the action over an iterator
  *
  *  @param value Object
  **/
  iterator( value ){}
}

module.exports = AbstractStylesTask;

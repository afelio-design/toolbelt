/**
 *  Style Dev
 *
 *  @version 2.0
 *  @author Alexandre Masy <hello@alexandremasy.com>
 **/
const AbstractStyleTask   =   require( __base + '/lib/abstract.styles');
const util                =   require('util');
const TaskName            =   require( __base + '/helpers/TaskName' );

/**
 *  View Task
 **/
class Task extends AbstractStyleTask
{
  /**
   *  Constructor
   *
   *  @param {Config} config
   *  @param {Gulp} gulp
   **/
  constructor(config, gulp)
  {
    super(config, gulp);
  }

  /**
  *  Return the description of the task
  *
  *  @return String
  **/
  get description() { return 'Build the styles for development'; };

  /**
  *  Return the name of the task
  *
  *  @return String
  **/
  get name() { return TaskName.STYLES_DEV; };

  /**
  *  Iterator Executor
  *  Execute the action over an iterator
  *
  *  @param value Object
  **/
  iterator( value )
  {
    if (!(value.enabled == undefined ? true : JSON.parse(value.enabled)))
    {
      p.warn('Skipping due to flag `enabled` present');
      return;
    }

    const path                =   require('path');

    var sources = value.sources;

    var output = value.output || '';
    output = path.join( this.globalOutput, output );

    var config = {};
    config.sourcemap = true;

    if (!sources || sources.length == 0)
    {
      p.error('Error: No sources for the styles');
    }

    const sass                =   require('gulp-sass');
    const autoprefixer        =   require('gulp-autoprefixer');
    const plumber             =   require('gulp-plumber');
    const connect             =   require('gulp-connect');

    return this.gulp.src(sources, {cwd:this.globalRoot})
    .pipe(plumber({errorHandler:this.onError}))
    .pipe(sass(config))
    .pipe(autoprefixer())
    .pipe(this.gulp.dest(output))
    .pipe(connect.reload());
  }
}

module.exports = Task;

/**
 *  Build Prod
 *
 *  @version 2.0
 *  @author Alexandre Masy <hello@alexandremasy.com>
 **/
const AbstractTask    =   require( __base + '/lib/abstract');
const TaskName        =   require( __base + '/helpers/TaskName' );

class Task extends AbstractTask
{

  /**
   *  Constructor
   *
   *  @param {Config} config
   *  @param {Gulp} gulp
   **/
  constructor(config, gulp)
  {
    super(config, gulp);
  }

  /**
  *  Return the list of dependencies
  *
  *  @return Array.<String>
  **/
  get deps (){ return [ TaskName.VIEWS, TaskName.STYLES, TaskName.LIBS, TaskName.STATICS ]; };

  /**
  *  Return the description of the task
  *
  *  @return String
  **/
  get description (){ return 'Build the production version of the project'; };

  /**
  *  Return the name of the task
  *
  *  @return String
  **/
  get name (){ return TaskName.BUILD_PROD; };

  /**
  *  Execute the task
  *
  *  @TODO handle exceptions
  **/
  execute(){}
}

module.exports = Task;

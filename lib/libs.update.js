/**
 *  Libs Update
 *
 *  @version 2.0
 *  @author Alexandre Masy <hello@alexandremasy.com>
 **/
const AbstractLibsTask    =   require( __base + '/lib/abstract.libs');
const util                =   require('util');
const TaskName            =   require( __base + '/helpers/TaskName' );

/**
 *  View Task
 **/
class Task extends AbstractLibsTask
{
  /**
   *  Constructor
   *
   *  @param {Config} config
   *  @param {Gulp} gulp
   **/
  constructor(config, gulp)
  {
    super(config, gulp);
  }

  /**
  *  Return the description of the task
  *
  *  @return String
  **/
  get description(){ return 'Build via update the application libraries for development'; };

  /**
  *  Return the name of the task
  *
  *  @return String
  **/
  get name(){ return TaskName.LIBS_UPDATE; };

  /**
  *  Iterator Executor
  *  Execute the action over an iterator
  *
  *  @param value Object
  **/
  iterator( value )
  {
    if (!(value.enabled == undefined ? true : JSON.parse(value.enabled)))
    {
      p.warn('Skipping due to flag `enabled` present');
      return;
    }

    const path                =   require('path');

    var name = value.name;
    var sources = value.sources;
    var output = value.output || '';
    output = path.join( this.globalOutput, output );

    if (!name)
    {
      p.error('Error: The name of the library is missing');
    }

    if (!sources || sources.length == 0)
    {
      p.error('Error: No sources for the library');
    }

    const plumber             =   require('gulp-plumber');
    const concat              =   require('gulp-concat');
    const connect             =   require('gulp-connect');
    const changed             =   require('gulp-changed');

    return this.gulp.src(sources, {cwd:this.globalRoot})
    .pipe(plumber({errorHandler:this.onError}))
    .pipe(changed(output))
    .pipe(concat(name))
    .pipe(this.gulp.dest(output))
    .pipe(connect.reload());
  }
}

module.exports = Task;

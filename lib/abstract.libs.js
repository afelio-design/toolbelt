/**
 *  View Task
 *
 *  @version 2.0
 *  @author Alexandre Masy <hello@alexandremasy.com>
 **/
const AbstractIteratorTask = require( __base + '/lib/abstract.iterator');

class AbstractLibsTask extends AbstractIteratorTask
{
  /**
   *  Constructor
   *
   *  @param {Config} config
   *  @param {Gulp} gulp
   **/
  constructor(config, gulp)
  {
    super(config, gulp);
  }

  /**
   *  Execute the task
   **/
  execute()
  {
    super.execute();
    this.iterate( 'libs' );
  }
}

module.exports = AbstractLibsTask;

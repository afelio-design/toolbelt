const AbstractTask    =   require( __base + '/lib/abstract');
const util            =   require('util');
const TaskName        =   require( __base + '/helpers/TaskName' );
const inquirer        =   require('inquirer');
const _               =   require('underscore');

/**
 *  View Task
 **/
class Task extends AbstractTask
{
  /**
   *  Constructor
   *
   *  @param {Config} config
   *  @param {Gulp} gulp
   **/
  constructor(config, gulp)
  {
    super(config, gulp);
  }

  /**
  *  Return the description of the task
  *
  *  @return String
  **/
  get description() { return 'Help you out with the configuration.'; }

  /**
  *  Return the name of the task
  *
  *  @return String
  **/
  get name() { return TaskName.CONFIG; }

  /**
  *  Execute the task
  *
  *  @TODO handle exceptions
  **/
  execute()
  {
  }
}

module.exports = Task;

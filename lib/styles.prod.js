/**
 *  Styles Prod
 *
 *  @version 2.0
 *  @author Alexandre Masy <hello@alexandremasy.com>
 **/
class Task extends require( __base + '/lib/abstract.styles')
{
  /**
   *  Constructor
   *
   *  @param {Config} config
   *  @param {Gulp} gulp
   **/
  constructor(config, gulp)
  {
    super(config, gulp);
  }

  /**
  *  Return the aliases for the task
  *
  *  @return String
  **/
  get aliases(){ return require( __base + '/helpers/TaskName' ).STYLES_PROD; };

  /**
  *  Return the description of the task
  *
  *  @return String
  **/
  get description(){ return 'Build the styles for production'; };

  /**
  *  Return the name of the task
  *
  *  @return String
  **/
  get name(){ return 'styles'; };

  /**
  *  Return the description of the task
  *
  *  @return String
  **/
  get description(){ return 'Compile the scss|sass files onto a production css'; };

  /**
  *  Iterator Executor
  *  Execute the action over an iterator
  *
  *  @param value Object
  **/
  iterator( value )
  {
    if (!(value.enabled == undefined ? true : JSON.parse(value.enabled)))
    {
      p.warn('Skipping due to flag `enabled` present');
      return;
    }

    const path                  =   require('path');

    var sources = value.sources;

    var output = value.output || '';
    output = path.join( this.globalOutput, output );

    var config = {};
    config.outputStyle = 'compressed';

    if (!sources || sources.length == 0)
    {
      p.error('Error: No sources for the styles');
    }

    const sass                  =   require('gulp-sass');
    const autoprefixer          =   require('gulp-autoprefixer');
    const plumber               =   require('gulp-plumber');

    return this.gulp.src(sources, {cwd:this.globalRoot})
    .pipe(plumber({errorHandler:this.onError}))
    .pipe(sass(config))
    .pipe(autoprefixer())
    .pipe(this.gulp.dest(output));
  }
}

module.exports = Task;

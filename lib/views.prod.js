/**
 *  Views Prod
 *
 *  @version 2.0
 *  @author Alexandre Masy <hello@alexandremasy.com>
 **/
const TaskName            =   require( __base + '/helpers/TaskName' );

class Task extends require( __base + '/lib/abstract.views')
{
  /**
   *  Constructor
   *
   *  @param {Config} config
   *  @param {Gulp} gulp
   **/
  constructor(config, gulp)
  {
    super(config, gulp);
  }

  /**
  *  Return the aliases for the task
  *
  *  @return String
  **/
  get aliases(){ return TaskName.VIEWS_PROD; };

  /**
  *  Return the description of the task
  *
  *  @return String
  **/
  get description(){ return 'Build the views for production'; };

  /**
  *  Return the name of the task
  *
  *  @return String
  **/
  get name(){ return TaskName.VIEWS; };

  /**
  *  Iterator Executor
  *  Execute the action over an iterator
  *
  *  @param value Object
  **/
  iterator( value )
  {
    if (!(value.enabled == undefined ? true : JSON.parse(value.enabled)))
    {
      p.warn('Skipping due to flag `enabled` present');
      return;
    }

    const path                =   require('path');
    let sources = value.sources;

    let output = value.output || '';
    output = path.join( this.globalOutput, output );

    if (!sources || sources.length == 0)
    {
      p.error('Error: No sources for the styles');
    }

    const plumber             =   require('gulp-plumber');
    const connect             =   require('gulp-connect');

    return this.gulp.src(sources, {cwd:this.globalRoot})
    .pipe(plumber({errorHandler:this.onError}))
    .pipe( this.pugEngine(value.basedir || __dirname) )
    .pipe(this.gulp.dest(output))
    .pipe(connect.reload());
  }


  /**
  *  Return the pug engine
  *
  *  @return gulp-pug
  **/
  pugEngine(basedir)
  {
    let rebase = this.config.get('global:rebase') || '/';

    let plugins = [require('../helpers/pugjs.basedir')({paths: basedir})];
    let filters = {'rebase':this.rebase.bind(this, rebase)};

    return require('gulp-pug')({
      pug: this.pug,
      locals: this.config.get('global'),
      pretty: true,
      plugins: plugins,
      filters: filters
    });
  }

  /**
  *  Rebase the website via the url in the configuration file
  **/
  rebase( url )
  {
    let ret = '\n\t\t';
    ret += '<base href="https://afelio-design.gitlab.io';
    ret += url.trim();
    ret += '" target="_self">'

    return ret;
  }
}

module.exports = Task;

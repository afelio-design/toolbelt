
const AbstractTask    =   require( __base + '/lib/abstract');
const util            =   require('util');
const TaskName        =   require( __base + '/helpers/TaskName' );

/**
 *  View Task
 **/
class Task extends AbstractTask
{
  /**
   *  Constructor
   *
   *  @param {Config} config
   *  @param {Gulp} gulp
   **/
  constructor(config, gulp)
  {
    super(config, gulp);
  }

  /**
  *  Return the description of the task
  *
  *  @return String
  **/
  get description(){ return 'Create a web server to distribute the output folder throught http (:8080)'; };

  /**
  *  Return the name of the task
  *
  *  @return String
  **/
  get name(){ return TaskName.CONNECT; };

  /**
  *  Execute the task
  *
  *  @TODO handle exceptions
  **/
  execute()
  {
    const connect         =   require('gulp-connect');

    var port = this.config.get( "global:port" ) || "8080";
    var host = this.config.get( "global:host" ) || "localhost";
    super.execute();
    connect.server({
      root:this.globalOutput,
      livereload: true,
      port: port,
      host: host
    });
  }
}


exports = module.exports = Task;

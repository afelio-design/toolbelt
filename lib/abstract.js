/**
*   AbstractTask
*
*   @version 2.1
*   @author Alexandre Masy <hello@alexandremasy.com>
**/
class AbstractTask
{
  /**
   *  Constructor
   *
   *  @param {Config} config
   *  @param {Gulp} gulp
   **/
  constructor(config, gulp)
  {
    this.config = config;
    this.gulp = gulp;

    this.globalRoot = this.config.get('global:root') || process.cwd();
    this.globalOutput = this.config.get('global:output') || process.cwd();
  }

  /**
  *  Return the aliases for the task
  *
  *  @return {String}
  **/
  get aliases() { return ''; };

  /**
  *  Return the name of the task
  *
  *  @return {String}
  **/
  get name() { throw new Error('Error: Please define a name for your task!'); };

  /**
  *  Return the list of dependencies
  *
  *  @return {Array.<String>}
  **/
  get deps() { return []; };

  /**
  *  Return the description of the task
  *
  *  @return {String}
  **/
  get description() { return '@TODO: Write a description for the task! <'+this.getName()+'>'; };

  /**
  *  Execute the task
  **/
  execute()
  {
    if(!this.config.valid)
    {
      this.error('Error: No '+__configFileName + __configFileExtension+' found');
    }
  };

  /**
  *  onError
  *  Handle the error management via plumber
  *
  *  @param {Object} err
  **/
  onError(err)
  {
    const notify  = require('gulp-notify');

    notify.onError({
      title: "Afelio",
      subtitle: "<%= error.file %>",
      message: "Error: <%= error.messageOriginal || error.msg || error.message %> in file <%= error.file || error.filename || error.path %>"
    })(err);

    console.log(err);

    this.emit('end');
  }

  /**
  *  Display an error message
  *
  *  @param {String} msg
  **/
  error(msg)
  {
    const chalk = require('chalk');

    console.log( chalk.red(msg) );
    process.exit(1);
  }

  /**
  *  Display an warning message
  *
  *  @param {String} msg
  **/
  warn(msg)
  {
    const chalk = require('chalk');

    console.log( chalk.yellow(msg) );
  }
}

module.exports = AbstractTask;

/**
 *  Clean
 *
 *  @version 2.0
 *  @author Alexandre Masy <hello@alexandremasy.com>
 **/
const AbstractTask    =   require( __base + '/lib/abstract');
const TaskName        =   require( __base + '/helpers/TaskName' );

/**
 *  View Task
 **/
class Task extends AbstractTask
{
  /**
   *  Constructor
   *
   *  @param {Config} config
   *  @param {Gulp} gulp
   **/
  constructor(config, gulp)
  {
    super(config, gulp);
  }

  /**
  *  Return the description of the task
  *
  *  @return String
  **/
  get description() { return 'Delete all the files in the output directory.'; };

  /**
  *  Return the name of the task
  *
  *  @return String
  **/
  get name() { return TaskName.CLEAN; };

  /**
  *  Execute the task
  *
  *  @TODO handle exceptions
  **/
  execute()
  {
    const del = require('del');

    super.execute();
    return del([
      this.globalOutput
    ]);
  }
}

module.exports = Task;

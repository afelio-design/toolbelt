/**
 *  Libs dev
 *
 *  @version 2.0
 *  @author Alexandre Masy
 **/
const AbstractLibsTask    =   require( __base + '/lib/abstract.libs');

/**
 *  View Task
 **/
class Task extends AbstractLibsTask
{
  /**
   *  Constructor
   *
   *  @param {Config} config
   *  @param {Gulp} gulp
   **/
  constructor(config, gulp)
  {
    super(config, gulp);
  }

  /**
  *  Return the description of the task
  *
  *  @return String
  **/
  get description() { return 'Build the application libraries for the development'; };

  /**
  *  Return the name of the task
  *
  *  @return String
  **/
  get name() { return require( __base + '/helpers/TaskName' ).LIBS_DEV; };

  /**
  *  Iterator Executor
  *  Execute the action over an iterator
  *
  *  @param value Object
  **/
  iterator( value )
  {
    const path                =   require('path');
    const plumber             =   require('gulp-plumber');
    const concat              =   require('gulp-concat');
    const connect             =   require('gulp-connect');

    if (!(value.enabled == undefined ? true : JSON.parse(value.enabled)))
    {
      this.warn('Skipping due to flag `enabled` present');
      return;
    }

    var name = value.name;
    var sources = value.sources;
    var output = value.output || '';
    output = path.join( this.globalOutput, output );

    if (!name)
    {
      this.error('Error: The name of the library is missing');
    }

    if (!sources || sources.length == 0)
    {
      this.error('Error: No sources for the library');
    }

    return this.gulp.src(sources, {cwd:this.globalRoot})
    .pipe(plumber({errorHandler:this.onError}))
    .pipe(concat(name))
    .pipe(this.gulp.dest(output))
    .pipe(connect.reload());
  }
}

module.exports = Task;

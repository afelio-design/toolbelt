/**
 *  Watch
 *
 *  @version 2.0
 *  @author Alexandre Masy <hello@alexandremasy.com>
 **/
const AbstractTask    =   require( __base + '/lib/abstract.iterator');
const util            =   require('util');
const TaskName        =   require( __base + '/helpers/TaskName' );
const watch           =   require('gulp-watch');

/**
 *  View Task
 **/
class Task extends AbstractTask
{
  /**
   *  Constructor
   *
   *  @param {Config} config
   *  @param {Gulp} gulp
   **/
  constructor(config, gulp)
  {
    super(config, gulp);
  }

  /**
  *  Return the description of the task
  *
  *  @return String
  **/
  get description(){ return 'Watch the sources files for a change.'; };

  /**
  *  Return the name of the task
  *
  *  @return String
  **/
  get name(){ return TaskName.WATCH; };

  /**
  *  Execute the task
  *
  *  @TODO handle exceptions
  **/
  execute()
  {
    ['libs', 'styles', 'views'].map(k => {
      this.current = k;
      var ret = {sources:[], actions:[]};

      this.iterate(k)
      .filter( (e) => { return e != undefined; } )
      .forEach(function(e)
      {
        ret.sources = ret.sources.concat(e.sources);
        ret.actions = e.actions;
      });
      ;

      this.gulp.watch( ret.sources, {cwd:this.globalRoot}, ret.actions );
      return ret;
    })
  }

  /**
  *  Iterator Execution
  *  Execute the action over an iterator
  *
  *  @param value Object
  **/
  iterator( value )
  {
    var sources = value.watched;
    if (!sources || sources.length == 0)
    {
      if (value.name)
      this.warn('Info: No watch for '+ value.name);
      return;
    }

    var actions = [];

    switch ( this.current )
    {
      case 'libs':
      if (value.update)
      {
        actions.push( TaskName.LIBS_DEV );
      }
      else
      {
        actions.push( TaskName.LIBS_DEV );
      }
      break;

      case 'styles':
      if (value.update)
      {
        actions.push( TaskName.STYLES_UPDATE );
      }
      else
      {
        actions.push( TaskName.STYLES_DEV );
      }
      break;

      case 'views':
      if (value.update)
      {
        actions.push( TaskName.VIEWS_UPDATE );
      }
      else
      {
        actions.push( TaskName.VIEWS_DEV );
      }
      break;
    }

    return {
      sources: sources,
      actions: actions
    }

    // return this.gulp.watch( sources, {cwd:this.globalRoot}, actions );
  }
}

module.exports = Task;

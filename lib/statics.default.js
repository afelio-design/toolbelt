/**
 *  Statics Default
 *
 *  @version 2.0
 *  @author Alexandre Masy <hello@alexandremasy.com>
 **/
const TaskName        =   require( __base + '/helpers/TaskName' );

class Task extends require( __base + '/lib/abstract.iterator')
{
  /**
   *  Constructor
   *
   *  @param {Config} config
   *  @param {Gulp} gulp
   **/
  constructor(config, gulp)
  {
    super(config, gulp);
  }

  /**
  *  Return the aliases for the task
  *
  *  @return String
  **/
  get aliases(){ return TaskName.STATICS_FILES; };

  /**
  *  Return the description of the task
  *
  *  @return String
  **/
  get description(){ return 'Copy the statics files in the output folder'; };

  /**
  *  Return the name of the task
  *
  *  @return String
  **/
  get name(){ return TaskName.STATICS; };

  /**
  *  Execute the task
  *
  *  @TODO handle exceptions
  *  @TODO Add iteration over multiple package
  *  @TODO Handle multiple package of just one
  **/
  execute()
  {
    super.execute();
    this.iterate('statics');
  }

  /**
  *  Iterator Executor
  *  Execute the action over an iterator
  *
  *  @param value Object
  **/
  iterator( value )
  {
    const path            =   require('path');

    var sources = value.sources;
    var output = value.output || '';
    output = path.join( this.globalOutput, output );

    return this.gulp.src(sources, {cwd:this.globalRoot})
    .pipe(this.gulp.dest(output));
  }
}

module.exports = Task;

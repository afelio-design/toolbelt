const AbstractTask    =   require( __base + '/lib/abstract');
const util            =   require('util');
const TaskName        =   require( __base + '/helpers/TaskName' );

/**
 *  View Task
 **/
class Task extends AbstractTask
{
  /**
   *  Constructor
   *
   *  @param {Config} config
   *  @param {Gulp} gulp
   **/
  constructor(config, gulp)
  {
    super(config, gulp);
  }

  /**
  *  Return the description of the task
  *
  *  @return String
  **/
  get description() { return 'Initialise the project. This command will create the '+__configFileName + __configFileExtension+' file'; }

  /**
  *  Return the name of the task
  *
  *  @return String
  **/
  get name() { return TaskName.INIT; }

  /**
  *  Execute the task
  *
  *  @TODO handle exceptions
  **/
  execute()
  {
    const path            =   require('path');
    const fs              =   require('fs');
    const chalk           =   require('chalk');

    var message = '';
    message += require(__base + '/helpers/logo');

    message += '\n';
    message += 'Congratulation! A '+chalk.yellow(__configFileName + __configFileExtension)+' file has been created!' + '\n';
    message += 'It is now the time to configure it. ';
    message += 'Refer to the documentation for the configuration: '+chalk.yellow('https://gitlab.com/afelio-design/toolbelt/wikis/configuration') + '\n';
    message += 'Use '+chalk.yellow('afelio --help')+' to know to use the toolbelt.' + '\n';

    var template = require( path.join(__base, 'templates/config.json') );
    var config = __configFileName + __configFileExtension;

    if (fs.existsSync(config))
    {
      console.log( chalk.red('Error: A '+__configFileName + __configFileExtension+' file already exists') );
      process.exit(-1);
    }

    fs.writeFile(config, JSON.stringify(template, null, 2), function(err)
    {
      if (err)
      {
        return console.log(err);
      }
      console.log( message );
    })
  }
}

module.exports = Task;

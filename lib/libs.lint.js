/**
 *  Libs link
 *
 *  @version 2.0
 *  @author Alexandre Masy <hello@alexandremasy.com>
 **/
class Task extends require( __base + '/lib/abstract.libs')
{
  /**
   *  Constructor
   *
   *  @param {Config} config
   *  @param {Gulp} gulp
   **/
  constructor(config, gulp)
  {
    super(config, gulp);
  }

  /**
  *  Return the description of the task
  *
  *  @return String
  **/
  get description() { return 'Lint the application libraries of the project'; };

  /**
  *  Return the name of the task
  *
  *  @return String
  **/
  get name() { return require( __base + '/helpers/TaskName' ).LIBS_LINT; };

  /**
  *  Iterator Executor
  *  Execute the action over an iterator
  *
  *  @param value Object
  **/
  iterator( value )
  {
    const jslint = require('gulp-jslint');

    let sources = value.sources;

    if (!sources || sources.length == 0)
    {
      this.error('Error: No sources for the library');
    }

    return this.gulp.src(sources, {cwd:this.globalRoot})
    .pipe(require('gulp-plumber')({errorHandler:this.onError}))
    .pipe(jslint())
    .pipe(jslint.reporter('default', errorsOnly))
    .pipe(jslint.reporter('stylish', options));
  }
}

module.exports = Task;

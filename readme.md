# Afelio Toolbelt

## Installation

```
npm install -g @afelio/toolbelt
```

## Update

```
npm update -g @afelio/toolbelt
```

## Initialisation

To initialise your project, place yourself to the root of the project and type `afelio init`. This command will create the .afelio file and place some default values. For more information on how to use the toolbelt type `afelio --help`

module.exports = function (options) {
  return {

    resolve(filename, source, pugOptions)
    {
      let out = filename
      const {dirname, resolve} = require('path');
      const {accessSync, constants: {R_OK}} = require('fs');
      const exists = filename => {
        try {
          accessSync(filename, R_OK);
          return true;
        } catch (err) {
          return false;
        }
      };

      let paths = typeof options.paths == "string" ? [options.paths] : options.paths;

      if (filename[0] === '/')
      {
        filename = filename.substr(1);
        if (!paths.some(path => exists(out = resolve(path, filename))))
        {
          throw new Error(`${filename} cannot be found in any paths`);
        }
      }
      else
      {
        if (!source) {
          throw new Error('the "filename" option is required to use includes and extends with "relative" paths');
        }

        out = resolve(dirname(source), filename);
      }

      return out;
    }
  };
};

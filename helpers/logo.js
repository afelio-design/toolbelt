var chalk = require('chalk');

function getLogo()
{
  var ret = '';

  ret += '  '+(chalk.blue('&&&&@@                             ')) + '\n';
  ret += '  '+(chalk.blue('&&&&&&&&&&&@                       ')) + '\n';
  ret += '  '+(chalk.blue('  &&&&&&&&&&&&&                    ')) + '\n';
  ret += '  '+(chalk.blue('          &&&&&&&&@                ')) + '\n';
  ret += '  '+(chalk.red('&&&@')+chalk.blue('         &&&&&&&@              ')) + '\n';
  ret += '  '+(chalk.red('&&&&&&&&&')+chalk.blue('       &&&&&&@            ')) + '\n';
  ret += '  '+(chalk.red('&&&&&&&&&&&&&')+chalk.blue('      &&&&&@          ')) + '\n';
  ret += '  '+(chalk.red('       &&&&&&&&')+chalk.blue('     &&&&&&         ')) + '\n';
  ret += '  '+(chalk.red('          &&&&&&&')+chalk.blue('     &&&&&        ')) + '\n';
  ret += '  '+('&&&&&@       '+chalk.red('&&&&&&')+chalk.blue('    &&&&&       ')) + '\n';
  ret += '  '+('&&&&&&&&&      '+chalk.red('&&&&&')+chalk.blue('    &&&&&      ')) + '\n';
  ret += '  '+('   &&&&&&&&     '+chalk.red('&&&&&')+chalk.blue('    &&&&&     ')) + '\n';
  ret += '  '+('      &&&&&&     '+chalk.red('&&&&&')+chalk.blue('    &&&&     ')) + '\n';
  ret += '  '+('        &&&&&     '+chalk.red('&&&&@')+chalk.blue('   &&&&&    ')) + '\n';
  ret += '  '+('         &&&&&    '+chalk.red('&&&&&')+chalk.blue('    &&&&    ')) + '\n';
  ret += '  '+('          &&&&     '+chalk.red('&&&&')+chalk.blue('    &&&&    ')) + '\n';

  return ret;
}

module.exports = getLogo();

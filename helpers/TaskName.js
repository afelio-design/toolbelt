/**
 *  Declare the tasks names
 **/
TaskName = () => '';
TaskName.BUILD_DEV    =   'build.dev';
TaskName.BUILD_PROD   =   'build.prod';
TaskName.CLEAN        =   'clean';
TaskName.CONNECT      =   'connect';
TaskName.INIT         =   'init';
TaskName.WATCH        =   'watch';
TaskName.SHOOT        =   'shoot';
TaskName.CONFIG        =  'config';

TaskName.LIBS         =   'libs';
TaskName.LIBS_PROD    =   'libs.prod';
TaskName.LIBS_DEV     =   'libs.dev';
TaskName.LIBS_UPDATE  =   'libs.update';
TaskName.LIBS_LINT    =   'libs.lint';
TaskName.LIBS_VENDORS =   'libs.vendors';

TaskName.STATICS      =   'statics';
TaskName.STATICS_FILES=   'statics.files';
TaskName.ICONS_FONT   =   'icons.font';

TaskName.STYLES       =   'styles';
TaskName.STYLES_PROD  =   'styles.prod';
TaskName.STYLES_DEV   =   'styles.dev';
TaskName.STYLES_UPDATE=   'styles.update';
TaskName.STYLES_LINT  =   'styles.lint';

TaskName.VIEWS        =   'views';
TaskName.VIEWS_PROD   =   'views.prod';
TaskName.VIEWS_DEV    =   'views.dev';
TaskName.VIEWS_UPDATE =   'views.update';

exports = module.exports = TaskName;

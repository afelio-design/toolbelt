var fs      = require('fs'),
    nconf   = require('nconf'),
    path    = require('path'),
    findConfig = require(__base + '/helpers/find_config'),
    buildConfigName = require(__base +'/helpers/build_config_name'),
    findCwd = require(__base + '/helpers/find_cwd'),
    chalk = require('chalk')
;


/**
 *  Configuration utility
 **/
function Config()
{
  this.valid = null;
}

/**
 *  Prototype
 **/
var p = Config.prototype;

/**
 *  Load the configuration
 **/
p.load = function()
{
  var opts = {};
  var cwd = findCwd(opts);

  // make a copy of search paths that can be mutated for this run
  var searchPaths = [];

  // if cwd was provided explicitly, only use it for searching config
  if (opts.cwd)
  {
    searchPaths = [cwd];
  }
  else
  {
    // otherwise just search in cwd first
    searchPaths.unshift(cwd);
  }

  // calculate the regex to use for finding the config file
  var configNameSearch = buildConfigName({
    configName: __configFileName,
    extensions: [__configFileExtension]
  });

  // calculate configPath
  var configPath = findConfig({
    configNameSearch: configNameSearch,
    searchPaths: searchPaths,
    configPath: opts.configPath
  });

  if ( !configPath )
  {
    // console.log( chalk.red('No design.afelio found') );
    // process.exit(1);
    this.valid = false;
  }
  else
  {
    nconf.argv()
    .env()
    .file({file:configPath});
    this.valid = true;
  }

  return this;
}

p.get = function(key)
{
  return nconf.get(key) || null;
}

module.exports = new Config;

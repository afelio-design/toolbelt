const path = require('path');

/**
 *  Task Helper
 *
 *  @param config Config
 *  @param gulp   Gulp
 *  @param commander  Commander
 **/
function Task( config, gulp, commander )
{
  this.config = config;
  this.gulp = gulp;
  this.commander = commander;
}

// Prototype
var p = Task.prototype;

/**
 *  Register the task in gulp
 *
 *  @param src String
 **/
p.register = function( src )
{
  var d = new (require( path.join(__base, src) ))( this.config, this.gulp );

  var aliases = d.aliases;
  var name = d.name;
  var description = d.description;
  var deps = d.deps;
  var execute = d.execute.bind(d);

  this.gulp.task( name, deps, execute );
  this.commander
        .command(name)
        .alias(aliases)
        .description(description)
        .action(this.execute.bind(this, name))
}

/**
 *  Execute a task by his name
 *
 *  @param name String
 **/
p.execute = function(name)
{
  if ( this.gulp.hasTask(name) )
  {
    this.gulp.start(name);
  }
  else
  {
    console.log('Strangely the command is not declared in gulp');
  }
}

module.exports = Task;
